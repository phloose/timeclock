package com.example.timeclock

import android.content.Intent
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextClock
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    object currentDate {
        var TIME: Long? = null
        var DAY: Int? = null
        var MONTH: Int? = null
        var YEAR: Int? = null
        var WEEK: Int? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setTextClockTime()
        setTextClockDate()

        val timeClockDB = TimeClockDatabaseHelper(this)

        val workStartButton = findViewById<Button>(R.id.button_work_start).apply {
            setOnClickListener {
                getTimeForEntry()
                if (timeClockDB.workDayExists()) {
                    return@setOnClickListener
                }

                try {
                   timeClockDB.insertWorkDayStart()
                } catch (e: SQLiteException) {
                    Log.e("insertWorkDayStart", e.toString())
                    return@setOnClickListener
                }

                try {
                    timeClockDB.newWorkDayDate()
                } catch (e: SQLiteException) {
                    Log.e("newWorkDayDate", e.toString())
                    return@setOnClickListener
                }
                it.isEnabled = false
            }
        }

        val workStopButton = findViewById<Button>(R.id.button_work_stop).apply {
            setOnClickListener {
                if (workStartButton.isEnabled) {
                    return@setOnClickListener
                }
                getTimeForEntry()
                try {
                    timeClockDB.updateWorkDayStop()
                } catch (e: SQLiteException) {
                    Log.e("updateWorkDayStop", e.toString())
                    return@setOnClickListener
                }
                try {
                    timeClockDB.updateBreaksSum()
                } catch (e: SQLiteException) {
                    Log.e("updateBreaksSum", e.toString())
                    return@setOnClickListener
                }
                workStartButton.isEnabled = true
            }
        }

        val breakStartButton = findViewById<Button>(R.id.button_break_start).apply {
            setOnClickListener {
                if (workStartButton.isEnabled) {
                    return@setOnClickListener
                }
                getTimeForEntry()
                try {
                    timeClockDB.insertBreakStart()
                } catch (e: SQLiteException) {
                    Log.e("updateBreakStart", e.toString())
                    return@setOnClickListener
                }
               it.isEnabled = false
            }
        }

        val breakStopButton = findViewById<Button>(R.id.button_break_stop).apply {
            setOnClickListener {
                if (breakStartButton.isEnabled) {
                    return@setOnClickListener
                }
                getTimeForEntry()
                try {
                    timeClockDB.updateBreakStop()
                } catch (e: SQLiteException) {
                    Log.e("updateBreakStop", e.toString())
                    return@setOnClickListener
                }
                breakStartButton.isEnabled = true
            }
        }
    }

    private fun getTimeForEntry() {
        val calendar = Calendar.getInstance()
        currentDate.TIME = System.currentTimeMillis()
        currentDate.DAY = calendar.get(Calendar.DAY_OF_MONTH)
        currentDate.MONTH = calendar.get(Calendar.MONTH) + 1
        currentDate.YEAR = calendar.get(Calendar.YEAR)
        currentDate.WEEK = calendar.get(Calendar.WEEK_OF_YEAR)
    }

    private fun setTextClockTime() {
        val textTime = findViewById<TextClock>(R.id.textclock_time)
        if (!textTime.is24HourModeEnabled) {
            textTime.format12Hour = null
        }
        textTime.format24Hour = "HH:mm:ss"
    }

    private fun setTextClockDate() {
        val textDate = findViewById<TextView>(R.id.text_date)
        val dateFormat = SimpleDateFormat("'KW: 'w '-' E dd MMMM YYYY")
        val currentDate =  dateFormat.format(Date())
        textDate.text = currentDate.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
