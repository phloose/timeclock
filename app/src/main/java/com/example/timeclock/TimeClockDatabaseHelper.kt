package com.example.timeclock

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log

object TimeClockDB: BaseColumns {
    const val DATABASE_NAME = "TimeClockDB"
    const val DATABASE_VERSION = 1
    object Date :BaseColumns {
        const val TABLE_NAME = "Date"
        const val COLUMN_DAY = "day"
        const val COLUMN_MONTH = "month"
        const val COLUMN_YEAR = "year"
        const val COLUMN_WEEK = "week"
        const val COLUMN_WORKDAY = "work_day"
    }
    object Workday: BaseColumns {
        const val TABLE_NAME = "WorkDay"
        const val COLUMN_ID = "id"
        const val COLUMN_WORK_START = "work_start"
        const val COLUMN_WORK_STOP = "work_stop"
        const val COLUMN_BREAK_SUM = "breaks_sum"
    }
    object Breaks: BaseColumns {
        const val TABLE_NAME = "Breaks"
        const val COLUMN_ID = "id"
        const val COLUMN_WORKDAY = "work_day"
        const val COLUMN_BREAK_START = "break_start"
        const val COLUMN_BREAK_STOP = "break_stop"
    }
}

private const val SQL_CREATE_TIMECLOCK_DB_DATE =
        "CREATE TABLE IF NOT EXISTS '${TimeClockDB.Date.TABLE_NAME}' (" +
                "'${TimeClockDB.Date.COLUMN_DAY}' INTEGER NOT NULL," +
                "'${TimeClockDB.Date.COLUMN_MONTH}' INTEGER NOT NULL," +
                "'${TimeClockDB.Date.COLUMN_YEAR}' INTEGER NOT NULL," +
                "'${TimeClockDB.Date.COLUMN_WEEK}' INTEGER NOT NULL," +
                "'${TimeClockDB.Date.COLUMN_WORKDAY}' INTEGER NOT NULL," +
                "PRIMARY KEY('${TimeClockDB.Date.COLUMN_DAY}','${TimeClockDB.Date.COLUMN_MONTH}','${TimeClockDB.Date.COLUMN_YEAR}','${TimeClockDB.Date.COLUMN_WEEK}')," +
                "FOREIGN KEY('${TimeClockDB.Date.COLUMN_WORKDAY}') REFERENCES '${TimeClockDB.Workday.TABLE_NAME}' ('${TimeClockDB.Workday.COLUMN_ID}')" +
                ")"

private const val SQL_DELETE_TIMECLOCK_DB_DATE =
        "DROP TABLE IF EXISTS '${TimeClockDB.Date.TABLE_NAME}'"

private const val SQL_CREATE_TIMECLOCK_DB_WORKDAY =
        "CREATE TABLE IF NOT EXISTS '${TimeClockDB.Workday.TABLE_NAME}' (" +
                "'${TimeClockDB.Workday.COLUMN_ID}' INTEGER NOT NULL PRIMARY KEY," +
                "'${TimeClockDB.Workday.COLUMN_WORK_START}' REAL," +
                "'${TimeClockDB.Workday.COLUMN_WORK_STOP}' REAL," +
                "'${TimeClockDB.Workday.COLUMN_BREAK_SUM}' REAL" +
                ")"

private const val SQL_DELETE_TIMECLOCK_DB_WORKDAY =
        "DROP TABLE IF EXISTS ${TimeClockDB.Workday.TABLE_NAME}"

private const val SQL_CREATE_TIMECLOCK_DB_BREAKS =
        "CREATE TABLE IF NOT EXISTS '${TimeClockDB.Breaks.TABLE_NAME}' (" +
                "'${TimeClockDB.Breaks.COLUMN_ID}' INTEGER NOT NULL PRIMARY KEY," +
                "'${TimeClockDB.Breaks.COLUMN_WORKDAY}' INTEGER," +
                "'${TimeClockDB.Breaks.COLUMN_BREAK_START}' REAL," +
                "'${TimeClockDB.Breaks.COLUMN_BREAK_STOP}' REAL," +
                "FOREIGN KEY('${TimeClockDB.Breaks.COLUMN_WORKDAY}') REFERENCES '${TimeClockDB.Workday.TABLE_NAME}' ('${TimeClockDB.Workday.COLUMN_ID}')" +
                ")"

private const val SQL_DELETE_TIMECLOCK_DB_BREAKS =
        "DROP TABLE IF EXISTS ${TimeClockDB.Breaks.TABLE_NAME}"

class TimeClockDatabaseHelper(context: Context) : SQLiteOpenHelper(context, TimeClockDB.DATABASE_NAME,null, TimeClockDB.DATABASE_VERSION){

    var workDayID: Long? = null
    var currentBreakID: Long? = null

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_TIMECLOCK_DB_WORKDAY)
        db?.execSQL(SQL_CREATE_TIMECLOCK_DB_DATE)
        db?.execSQL(SQL_CREATE_TIMECLOCK_DB_BREAKS)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_TIMECLOCK_DB_DATE)
        db?.execSQL(SQL_DELETE_TIMECLOCK_DB_WORKDAY)
        db?.execSQL(SQL_DELETE_TIMECLOCK_DB_BREAKS)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    fun insertWorkDayStart(): Long {
        val workDayValues = ContentValues().apply {
            put(TimeClockDB.Workday.COLUMN_WORK_START, MainActivity.currentDate.TIME)
        }
        Log.d("insertWorkDayStart", workDayValues.toString())
        workDayID =  writableDatabase.insert(
                TimeClockDB.Workday.TABLE_NAME,
                null,
                workDayValues
        )
        return workDayID as Long
    }

    fun updateWorkDayStop() {
        val workDayValues = ContentValues().apply {
            put(TimeClockDB.Workday.COLUMN_WORK_STOP, MainActivity.currentDate.TIME)
        }
        Log.d("updateWorkDayStop", workDayValues.toString())
        writableDatabase.update(
                TimeClockDB.Workday.TABLE_NAME,
                workDayValues,
                "${TimeClockDB.Workday.COLUMN_ID} LIKE ?",
                arrayOf(workDayID.toString())
        )
    }

    fun insertBreakStart(): Long {
        val workDayValues = ContentValues().apply {
            put(TimeClockDB.Breaks.COLUMN_BREAK_START, MainActivity.currentDate.TIME)
            put(TimeClockDB.Breaks.COLUMN_WORKDAY, workDayID)
        }
        Log.d("insertBreakStart", workDayValues.toString())
        currentBreakID = writableDatabase.insert(
                TimeClockDB.Breaks.TABLE_NAME,
                null,
                workDayValues
        )
        return currentBreakID as Long
    }

    fun updateBreakStop() {
        val workDayValues = ContentValues().apply {
            put(TimeClockDB.Breaks.COLUMN_BREAK_STOP, MainActivity.currentDate.TIME)
        }
        Log.d("updateBreakStop", workDayValues.toString())
        writableDatabase.update(
                TimeClockDB.Breaks.TABLE_NAME,
                workDayValues,
                "${TimeClockDB.Breaks.COLUMN_ID} LIKE ?",
                arrayOf(currentBreakID.toString())
        )
    }

    fun updateBreaksSum() {
        val cursor = writableDatabase.rawQuery(
                "SELECT sum( break_stop - break_start) as breaks_sum FROM " +
                        "${TimeClockDB.Breaks.TABLE_NAME} WHERE " +
                        "${TimeClockDB.Breaks.COLUMN_WORKDAY} = $workDayID",
                null
        )
        var breaksSum : Float? = null
        if (cursor.moveToFirst()) {
            breaksSum = cursor.getFloat(cursor.getColumnIndex("breaks_sum"))
        } else  {
            throw SQLiteException("Error while summing up break times!")
        }
        val workDayValues = ContentValues().apply {
            put(TimeClockDB.Workday.COLUMN_BREAK_SUM, breaksSum)
        }
        Log.d("updateBreaksSum", workDayValues.toString())
        writableDatabase.update(
                TimeClockDB.Workday.TABLE_NAME,
                workDayValues,
                "${TimeClockDB.Workday.COLUMN_ID} LIKE ?",
                arrayOf(workDayID.toString())
        )
    }

    fun newWorkDayDate(): Long {
        val dateValues = ContentValues().apply {
            put(TimeClockDB.Date.COLUMN_DAY, MainActivity.currentDate.DAY)
            put(TimeClockDB.Date.COLUMN_MONTH, MainActivity.currentDate.MONTH)
            put(TimeClockDB.Date.COLUMN_YEAR, MainActivity.currentDate.YEAR)
            put(TimeClockDB.Date.COLUMN_WEEK, MainActivity.currentDate.WEEK)
            put(TimeClockDB.Date.COLUMN_WORKDAY, workDayID)
        }
        Log.d("newWorkDayDate", dateValues.toString())
        return writableDatabase.insert(
                TimeClockDB.Date.TABLE_NAME,
                null,
                dateValues
        )
    }

    fun workDayExists(): Boolean {
        val cursor = writableDatabase.rawQuery(
                "SELECT * FROM ${TimeClockDB.Date.TABLE_NAME} WHERE " +
                        "${TimeClockDB.Date.COLUMN_DAY} = ${MainActivity.currentDate.DAY} AND " +
                        "${TimeClockDB.Date.COLUMN_MONTH} = ${MainActivity.currentDate.MONTH} AND " +
                        "${TimeClockDB.Date.COLUMN_YEAR} = ${MainActivity.currentDate.YEAR} AND " +
                        "${TimeClockDB.Date.COLUMN_WEEK} = ${MainActivity.currentDate.WEEK}",
                null
        )
        return cursor.count > 0
    }
}